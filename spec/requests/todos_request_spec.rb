require 'rails_helper'

RSpec.describe "Todos", type: :request do
  let!(:todos) { create_list(:todo, 10) }

  describe 'GET /todos' do
    context 'TODO一覧取得に成功したとき' do
      example '一覧取得成功時のレスポンスを返す' do
        get '/todos'

        expect(json).to be_present
        expect(json['todos'].size).to eq Todo.count
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
        expect(response.status).to eq 200
      end
    end

    context 'TODO一覧取得に失敗したとき' do
      example '一覧取得失敗時のレスポンスを返す' do
        allow(Todo).to receive(:select).and_raise(ActiveRecord::ActiveRecordError)
        get '/todos'

        expect(json['error_code']).to eq 3
        expect(json['error_message']).to eq '一覧の取得に失敗しました'
        expect(response.status).to eq 500
      end
    end

    context 'サーバーエラーが発生したとき' do
      example 'サーバー内エラー発生時のレスポンスを返す' do
        allow(Todo).to receive(:select).and_raise(Exception)
        get '/todos'

        expect(json['error_code']).to eq 1
        expect(json['error_message']).to eq 'サーバー内で不明なエラーが発生しました'
        expect(response.status).to eq 500
      end
    end
  end

  describe 'POST /todos' do
    let(:valid_attributes) { { title: 'test', detail: 'test', date: '2020-01-01' } }

    context 'TODO登録のリクエストが正当なとき' do
      example 'リクエスト正当時のレスポンスを返す' do
        post '/todos', params: valid_attributes

        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
        expect(response.status).to eq 200
      end
    end

    context 'TODO登録のtitle文字数が100丁度で正当なリクエストであるとき' do
      let(:params) { { title: 't' * 100, detail: 'test' } }

      example 'リクエスト正当時のレスポンスを返す' do
        post '/todos', params: params

        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
        expect(response.status).to eq 200
      end
    end

    context 'TODO登録のdetail文字数が1000丁度で正当なリクエストであるとき' do
      let(:params) { { title: 'test', detail: 't' * 1000 } }

      example 'リクエスト正当時のレスポンスを返す' do
        post '/todos', params: params

        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
        expect(response.status).to eq 200
      end
    end

    context 'TODO登録のdetailがnilのとき' do
      let(:params) { { title: 'test', detail: nil } }

      example 'リクエスト正当時のレスポンスを返す' do
        post '/todos', params: params

        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
        expect(response.status).to eq 200
      end
    end

    context 'TODO登録のdateがnilのとき' do
      let(:params) { { title: 'test', date: nil } }

      example 'リクエスト正当時のレスポンスを返す' do
        post '/todos', params: params

        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
        expect(response.status).to eq 200
      end
    end

    context 'TODO登録のリクエストが不正なとき' do
      let(:invalid_attributes) { { title: '', detail: 'test', date: '2020-01-01' } }
      example 'リクエスト不正時のレスポンスを返す' do
        post '/todos', params: invalid_attributes

        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエスト形式が不正です'
        expect(response.status).to eq 400
      end
    end

    context 'TODO登録のtitle文字数が100より多くて不正なリクエストであるとき' do
      let(:params) { { title: 't' * 101 } }

      example 'リクエスト不正時のレスポンスを返す' do
        post '/todos', params: params

        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエスト形式が不正です'
        expect(response.status).to eq 400
      end
    end

    context 'TODO登録のdetail文字数が1000より多くて不正なリクエストであるとき' do
      let(:params) { { title: 'test', detail: 't' * 1001 } }

      example 'リクエスト不正時のレスポンスを返す' do
        post '/todos', params: params

        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエスト形式が不正です'
        expect(response.status).to eq 400
      end
    end

    context 'TODO登録に失敗したとき' do
      example '登録失敗時のレスポンスを返す' do
        allow(Todo).to receive(:create!).and_raise(ActiveRecord::ActiveRecordError)
        post '/todos', params: valid_attributes

        expect(json['error_code']).to eq 4
        expect(json['error_message']).to eq '登録に失敗しました'
        expect(response.status).to eq 500
      end
    end

    context 'サーバーエラーが発生したとき' do
      example 'サーバー内エラー発生時のレスポンスを返す' do
        allow(Todo).to receive(:create!).and_raise(Exception)
        post '/todos', params: valid_attributes

        expect(json['error_code']).to eq 1
        expect(json['error_message']).to eq 'サーバー内で不明なエラーが発生しました'
        expect(response.status).to eq 500
      end
    end
  end

  describe 'PUT /todos' do
    let(:valid_attributes) { { detail: 'test', date: '2020-06-09' } }
    let(:todo_id) { todos.first.id }

    context 'TODO更新のリクエストが正当のとき' do
      example 'リクエスト正当時のレスポンスを返す' do
        put "/todos/#{todo_id}", params: valid_attributes

        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
        expect(response.status).to eq 200
      end
    end

    context 'TODO更新のtitle文字数が100丁度で正当なリクエストであるとき' do
      let(:params) { { title: 't' * 100 } }

      example 'リクエスト正当時のレスポンスを返す' do
        put "/todos/#{todo_id}", params: params

        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
        expect(response.status).to eq 200
      end
    end

    context 'TODO更新のdetail文字数が1000丁度で正当なリクエストであるとき' do
      let(:params) { { detail: 't' * 1000 } }

      example 'リクエスト正当時のレスポンスを返す' do
        put "/todos/#{todo_id}", params: params

        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
        expect(response.status).to eq 200
      end
    end

    context 'TODO更新のdetailがnilのとき' do
      let(:params) { { detail: nil } }

      example 'リクエスト正当時のレスポンスを返す' do
        put "/todos/#{todo_id}", params: params

        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
        expect(response.status).to eq 200
      end
    end

    context 'TODO更新のdateがnilのとき' do
      let(:params) { { date: nil } }

      example 'リクエスト正当時のレスポンスを返す' do
        put "/todos/#{todo_id}", params: params

        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
        expect(response.status).to eq 200
      end
    end

    context 'TODO更新のリクエストが不正なとき' do
      let(:invalid_attributes) { { title: '', detail: 'test', date: '2020-06-09' } }

      example 'リクエスト不正時のレスポンスを返す' do
        put "/todos/#{todo_id}", params: invalid_attributes

        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエスト形式が不正です'
        expect(response.status).to eq 400
      end
    end

    context 'TODO更新のtitle文字数が100より多くて不正なリクエストであるとき' do
      let(:params) { { title: 't' * 101, detail: 'test' } }

      example 'リクエスト不正時のレスポンスを返す' do
        put "/todos/#{todo_id}", params: params

        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエスト形式が不正です'
        expect(response.status).to eq 400
      end
    end

    context 'TODO更新のdetail文字数が1000より多くて不正なリクエストであるとき' do
      let(:params) { { title: 'test', detail: 't' * 1001 } }

      example 'リクエスト不正時のレスポンスを返す' do
        put "/todos/#{todo_id}", params: params

        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエスト形式が不正です'
        expect(response.status).to eq 400
      end
    end

    context 'TODO更新に失敗したとき' do
      example '更新失敗時のレスポンスを返す' do
        allow_any_instance_of(Todo).to receive(:update!).and_raise(ActiveRecord::ActiveRecordError)
        put "/todos/#{todo_id}", params: valid_attributes

        expect(json['error_code']).to eq 5
        expect(json['error_message']).to eq '更新に失敗しました'
        expect(response.status).to eq 500
      end
    end

    context 'サーバーエラーが発生したとき' do
      example 'サーバー内エラー発生時のレスポンスを返す' do
        allow_any_instance_of(Todo).to receive(:update!).and_raise(Exception)
        put "/todos/#{todo_id}"

        expect(json['error_code']).to eq 1
        expect(json['error_message']).to eq 'サーバー内で不明なエラーが発生しました'
        expect(response.status).to eq 500
      end
    end
  end

  describe 'DELETE /todos' do
    let(:todo_id) { todos.first.id }

    context 'TODO削除に成功したとき' do
      example '削除成功時のレスポンスを返す' do
        delete "/todos/#{todo_id}"

        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
        expect(response.status).to eq 200
      end
    end

    context '削除対象のidが存在しないとき' do
      before { Todo.find(todo_id).destroy }

      example 'リクエスト不正時のレスポンスを返す' do
        delete "/todos/#{todo_id}"

        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエスト形式が不正です'
        expect(response.status).to eq 400
      end
    end

    context 'TODO削除に失敗したとき' do
      example '削除失敗時のレスポンスを返す' do
        allow_any_instance_of(Todo).to receive(:destroy!).and_raise(ActiveRecord::ActiveRecordError)
        delete "/todos/#{todo_id}"

        expect(json['error_code']).to eq 6
        expect(json['error_message']).to eq '削除に失敗しました'
        expect(response.status).to eq 500
      end
    end

    context 'サーバーエラーが発生したとき' do
      example 'サーバー内エラー発生時のレスポンスを返す' do
        allow_any_instance_of(Todo).to receive(:destroy!).and_raise(Exception)
        delete "/todos/#{todo_id}"

        expect(json['error_code']).to eq 1
        expect(json['error_message']).to eq 'サーバー内で不明なエラーが発生しました'
        expect(response.status).to eq 500
      end
    end
  end
end