class TodosController < ApplicationController
  before_action :set_todo, only: [:update, :destroy]

  NO_ERROR_CODE = 0
  BAD_REQUEST_ERROR_CODE = 2
  INDEX_ERROR_CODE = 3
  CREATE_ERROR_CODE = 4
  UPDATE_ERROR_CODE = 5
  DELETE_ERROR_CODE = 6

  # GET /todos
  def index
    todos = Todo.select(:id, :title, :detail, :date).order(:date)
    render json: base_response_model.merge({ todos: todos }), status: 200
  rescue ActiveRecord::ActiveRecordError
    render json: base_response_model(error_code: INDEX_ERROR_CODE, error_message: '一覧の取得に失敗しました'), status: 500
  end

  # POST /todos
  def create
    Todo.create!(todo_params)
    render json: base_response_model, status: 200
  rescue ActiveRecord::RecordInvalid
    render_bad_request
  rescue ActiveRecord::ActiveRecordError
    render json: base_response_model(error_code: CREATE_ERROR_CODE, error_message: '登録に失敗しました'), status: 500
  end

  # PUT /todos/:id
  def update
    @todo.update!(todo_params)
    render json: base_response_model, status: 200
  rescue ActiveRecord::RecordInvalid
    render_bad_request
  rescue ActiveRecord::ActiveRecordError
    render json: base_response_model(error_code: UPDATE_ERROR_CODE, error_message: '更新に失敗しました'), status: 500
  end

  # DELETE /todos/:id
  def destroy
    @todo.destroy!
    render json: base_response_model, status: 200
  rescue ActiveRecord::ActiveRecordError
    render json: base_response_model(error_code: DELETE_ERROR_CODE, error_message: '削除に失敗しました'), status: 500
  end

  private

  def todo_params
    params.permit(:title, :detail, :date)
  end

  def set_todo
    @todo = Todo.find(params[:id])
  rescue ActiveRecord::ActiveRecordError
    render_bad_request
  end

  def base_response_model(error_code: NO_ERROR_CODE, error_message: '')
    { error_code: error_code, error_message: error_message }
  end

  def render_bad_request
    render json: base_response_model(error_code: BAD_REQUEST_ERROR_CODE, error_message: 'リクエスト形式が不正です'), status: 400
  end
end