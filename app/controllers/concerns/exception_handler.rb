module ExceptionHandler
  extend ActiveSupport::Concern

  included do
    rescue_from Exception, with: :render_internal_server_error
  end

  INTERNAL_SERVER_ERROR_CODE = 1

  def render_internal_server_error
    render json: { error_code: INTERNAL_SERVER_ERROR_CODE, error_message: 'サーバー内で不明なエラーが発生しました' }, status: 500
  end
end